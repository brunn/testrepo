#property description "Moving Average sample expert advisor"
#define MAGICMA  20131111
input double Lots          =1;
input double MaximumRisk   =2;
input double DecreaseFactor=3;
input int    MovingPeriod  =12;
input int    MovingShift   =6;
int CalculateCurrentOrders(string symbol){
   int buys=0,sells=0;
   for(int i=0;i<OrdersTotal();i++){
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false) break;
      if(OrderSymbol()==Symbol() && OrderMagicNumber()==MAGICMA)
        {
         if(OrderType()==OP_BUY)  buys++;
         if(OrderType()==OP_SELL) sells++;
        }
     }
   if(buys>0) return(buys);
   else       return(-sells);
  }
